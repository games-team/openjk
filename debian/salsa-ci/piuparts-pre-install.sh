#!/bin/sh
# Workaround for https://bugs.debian.org/1010967,
# https://bugs.debian.org/1084173
echo 'APT::Get::allow-remove-essential "true";' >> /etc/apt/apt.conf.d/1010967.conf
